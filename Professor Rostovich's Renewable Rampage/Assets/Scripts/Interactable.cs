﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//base class for interactable objects in the scene to allow an interface for interaction
public class Interactable : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public virtual void DoAction()
	{
		
	}

	public virtual void StopAction()
	{
		
	}
}
