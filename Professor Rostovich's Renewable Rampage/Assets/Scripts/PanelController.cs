﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : Interactable {
	int health = 100;
	SolarController cont;

	// Use this for initialization
	void Start () {
		cont = GetComponentInParent<SolarController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void DoAction()
	{
		if(!cont.IsActive())
		{
			return;
		}

		print("panel hit");
	}
}
