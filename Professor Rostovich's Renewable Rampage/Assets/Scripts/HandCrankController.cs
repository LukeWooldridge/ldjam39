﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandCrankController : Interactable {
	public HandCrankUI ui;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void DoAction(){
		ui.gameObject.SetActive(true);
		ui.timeTillNextPull = 0.0f;
		Object.FindObjectOfType<PlayerController>().setMovement(false);
	}

	public override void StopAction(){

		ui.gameObject.SetActive(false);
		Object.FindObjectOfType<PlayerController>().setMovement(true);
	}
}
