﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script that controls player movement and action


public class PlayerController : MonoBehaviour {
	struct Movement{
		public bool move;
		public float forward;
		public float right;
		public float lookUp;
		public float lookRight;
	}
	Movement movement; 
	Rigidbody rb;
	Camera camera;

	public float lookSpeed = 50.0f;
	public float moveSpeed = 2.0f;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		camera = Camera.main;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		movement.move = true;
	}
	
	// Update is called once per frame
	void Update () {
		//reset movement values so that they are properly recored
		movement.forward = 0.0f;
		movement.right = 0.0f;

		if(Input.GetKey(KeyCode.W))
		{
			movement.forward = 1.0f;
		}else if (Input.GetKey(KeyCode.S))
		{
			movement.forward = -1.0f;
		}

		if(Input.GetKey(KeyCode.A))
		{
			movement.right = -1.0f;
		}else if (Input.GetKey(KeyCode.D))
		{
			movement.right = 1.0f;
		}

		//adjust movement values for the curret frame update time
		movement.right = movement.right * moveSpeed * Time.deltaTime;
		movement.forward = movement.forward * moveSpeed * Time.deltaTime;

		movement.lookUp = -(Input.GetAxis("Mouse Y") * Time.deltaTime);
		movement.lookRight = Input.GetAxis("Mouse X") * Time.deltaTime;

		if(movement.move)
		{
			Move();
			Look();

			if(Input.GetButtonDown("Fire1"))
			{
				Click();
			}
		}
	}

	void Move()
	{
		rb.MovePosition(transform.position + (transform.right * movement.right) + (transform.forward * movement.forward));
	}

	void Look()
	{
		//we will want to control Y rotation from the rigidbody, but rotation around the X coord will want to be done o the camera
		Vector3 yRot = new Vector3(0.0f, movement.lookRight, 0.0f);

		Quaternion deltaRotation = Quaternion.Euler(yRot * lookSpeed);

		rb.MoveRotation(rb.rotation * deltaRotation);

		Vector3 xRot = new Vector3(movement.lookUp, 0.0f, 0.0f);
		deltaRotation = Quaternion.Euler(xRot * lookSpeed);

		camera.transform.rotation = camera.transform.rotation * deltaRotation;
	}

	void Click()
	{
		RaycastHit hit;
		float maxDistance = 2.0f;
		if(Physics.Raycast(camera.transform.position, camera.transform.forward,out hit, maxDistance))
		{
			Interactable target = hit.transform.GetComponent<Interactable>();
			if(target)
			{
				target.DoAction();
			}
		}

	}

	public void setMovement(bool active)
	{
		movement.move = active;
	}
}
