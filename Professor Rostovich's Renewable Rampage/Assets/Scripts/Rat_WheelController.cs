﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rat_WheelController : Interactable {
	public RatWheelUI ui;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void DoAction(){
		ui.gameObject.SetActive(true);
		Object.FindObjectOfType<PlayerController>().setMovement(false);
	}

	public override void StopAction(){

		ui.gameObject.SetActive(false);
		Object.FindObjectOfType<PlayerController>().setMovement(true);
	}
}
