﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatWheelUI : MonoBehaviour {
	bool active;
	// Use this for initialization
	public UnityEngine.UI.Button aButton;
	public UnityEngine.UI.Button dButton;
	public Color activeColor;
	public Color inactiveColor;
	public StatsTracker stats;
	public Interactable invoker;

	KeyCode nextKey = KeyCode.A;
	void Start () {
		changeColor(aButton, activeColor);
		changeColor(dButton, inactiveColor);
	}

	void changeColor(UnityEngine.UI.Button butt, Color col)
	{
		var cb = butt.colors;
		cb.disabledColor = col;
		butt.colors = cb;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			//player wants to exit machine
			invoker.StopAction();
			return;
		}

		if(Input.GetKeyDown(nextKey))
		{
			if(nextKey == KeyCode.A)
			{
				nextKey = KeyCode.D;
				changeColor(aButton, inactiveColor);
				changeColor(dButton, activeColor);
			}else{
				nextKey = KeyCode.A;
				changeColor(aButton, activeColor);
				changeColor(dButton, inactiveColor);
			}
			if(stats.acutatorIncrement())
			{
				//the acutators are fully charged, time to hop off the machine
				invoker.StopAction();
				return;
			}
		}

	}
}
