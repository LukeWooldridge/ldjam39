﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsTracker : MonoBehaviour {
	float moveEnergy = 100.0f;
	float tickDuration = 2.0f;
	float currentTickTime = 0.0f;
	bool punching = true;
	float punchCharge = 0.0f;
	public UnityEngine.UI.Text moveText;
	public UnityEngine.UI.Text punchText;
	// Use this for initialization
	void Start () {
		
	}

	void Update()
	{
		moveText.text = "Acutator Energy: " + moveEnergy.ToString("F2");
		if(punching)
		{
			punchText.text = "Punch Charge: " + punchCharge.ToString("F2");
		}else{
			punchText.text = "Punch Charge: Standby";
		}

	}

	void FixedUpdate(){
		currentTickTime += Time.fixedDeltaTime;
		if(currentTickTime >= tickDuration)
		{
			currentTickTime = 0.0f;
			TickUpdate();
		}
	}

	//called every time a defined update time is passed
	void TickUpdate()
	{
		moveEnergy -= 0.5f;
	}

	//adds power to the leg acutator component
	public bool acutatorIncrement()
	{
		moveEnergy += 0.1f;
		if(moveEnergy >= 100.0f)
		{
			moveEnergy = 100.0f;
			return true;
		}
		return false;
	}

	public bool punchIncrement()
	{
		if(punching)
		{
			punchCharge += 10.0f;
			if(punchCharge >= 100.0f)
			{
				punchCharge = 0.0f;
				punching = false;
				return true;
			}else{
				return false;
			}
		}
		return true;
	}
}
