﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandCrankUI : MonoBehaviour {

	public StatsTracker stats;
	public Interactable invoker;
	public float pullWaitTime = 2.0f;
	public float pullWindow = 0.5f;
	public float timeTillNextPull = 0.0f;
	public UnityEngine.UI.Text txt;
	// Use this for initialization
	void Start () {
		txt.text = "wait " + (pullWaitTime - timeTillNextPull).ToString("F2");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			invoker.StopAction();
			return;
		}

		timeTillNextPull += Time.deltaTime;
		if(timeTillNextPull >= pullWaitTime)
		{
			Pull();
		}else{
			//if its not time to pull yet and the player hits space, that counts as a miss
			if(Input.GetKeyDown(KeyCode.Space))
			{
				timeTillNextPull = -1.0f;
			}

			if(timeTillNextPull < 0.0f)
			{
				txt.text = "missed!";
			}else
			{
				txt.text = "wait " + (pullWaitTime - timeTillNextPull).ToString("F2");				
			}
		}
	}

	void Pull()
	{
		if(timeTillNextPull <= (pullWaitTime + pullWindow))
		{
			//TODO: implement mashing here for more user engagement?
			txt.text = "Pull!";
			if(Input.GetKeyDown(KeyCode.Space))
			{
				//space has been presseed in time
				if(stats.punchIncrement())
				{
					//we have a fully charged punch
					invoker.StopAction();
					return;
				}
				timeTillNextPull = 0.0f;
			}
		}else{
			//pull window missed, reset
			timeTillNextPull = -1.0f;
		}
	}
}
