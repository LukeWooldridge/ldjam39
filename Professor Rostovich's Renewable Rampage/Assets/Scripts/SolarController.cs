﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarController : Interactable {
	bool exposed;
	bool rotating;
	float target;
	float closed = 210.0f;
	float open = 340.0f;
	public float speed = 4.0f;
	Transform panel;

	// Use this for initialization
	void Start () {
		exposed = false;
		rotating = false;
		panel  = transform.Find("solar");
	}
	
	// Update is called once per frame
	void Update () {
		if(rotating)
		{
			var rot = panel.rotation.eulerAngles;
			if(target > rot.z)
			{
				panel.Rotate(Vector3.forward, speed * Time.deltaTime);
				if(target <= panel.rotation.eulerAngles.z)
				{
					rot.z = target;
					rotating = false;
					exposed = true;
					panel.rotation = Quaternion.Euler(rot.x,rot.y,rot.z);
				}
			}else{
				panel.Rotate(Vector3.forward, (-speed) * Time.deltaTime);
				if(target >= panel.rotation.eulerAngles.z)
				{
					rot.z = target;
					rotating = false;
					exposed = false;
					panel.rotation = Quaternion.Euler(rot.x,rot.y,rot.z);
				}
			}
		}
	}

	public override void DoAction(){
		if(!rotating)
		{
			rotating = true;
			if(exposed)
			{
				target = closed;
				exposed = false;
			}else{
				target = open;
			}
		}
	}

	public bool IsActive()
	{
		return exposed;
	}
}
